
/* importing core libraries from angular */
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { SideBarComponent } from '../components/sidebar/sidebar.component';
import { HttpModule } from '@angular/http';
import {TableModule} from 'primeng/table';
import {InputTextareaModule} from 'primeng/inputtextarea';
import {DialogModule} from 'primeng/dialog';
import {ButtonModule} from 'primeng/button';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    TableModule,
    InputTextareaModule,
    DialogModule,
    ButtonModule,
    ReactiveFormsModule
  ],
  exports: [
    HttpModule,
    CommonModule,
    FormsModule,
    HttpClientModule,
    SideBarComponent,
    TableModule,
    InputTextareaModule,
    DialogModule,
    ButtonModule,
    ReactiveFormsModule
  ],
  declarations: [
    SideBarComponent
  ],
  providers: [
  ]
})

export class SharedModule { }

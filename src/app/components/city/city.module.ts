import { NgModule } from '@angular/core';
import { CityComponent } from './city.component';
import { routing } from './city.routing';
import { SharedModule } from 'src/app/shared/shared.module';
import { CityService } from './shared/city.service';
import { CityDialogComponent } from './city-dialog/city-dialog.component';

@NgModule({
    imports: [
        SharedModule,
        routing
    ],
    declarations: [
        CityComponent,
        CityDialogComponent

    ],
    providers: [
        CityService
    ]
})

export class CityModule { }

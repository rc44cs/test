
import { Router, NavigationEnd, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Component, OnInit, ViewChild } from '@angular/core';
import { CityService } from './shared/city.service';
import { CityModel } from './shared/city.model';

@Component({
    selector: 'app-city',
    templateUrl: './city.component.html',
})

export class CityComponent implements OnInit {
    cityList: Array<CityModel> = [];
    cityDetail: CityModel = new CityModel;
    displayCityDialog: boolean;

    constructor(
        private cityService: CityService
    ) {
    }

    ngOnInit() {
        this.getCityList()
    }

    getCityList() {
        this.cityService.getCityList().subscribe(result => {
            this.cityList = result.data.cityList;
        });
    }

    onSelectCity(event: CityModel) {
        this.cityDetail = event;
    }

    delete(city) {
        const index = this.cityList.indexOf(city)
        this.cityList.splice(index, 1);
    }

    addCity() {
        this.displayCityDialog = true
    }


    cancelEvent() {
        this.displayCityDialog = false;
    }

    saveCityEvent(event) {
        event.id = this.cityList.length + 1;
        this.cityList.push(event);
    }

}

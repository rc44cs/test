
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { CityModel } from '../shared/city.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
    selector: 'app-city-dialog',
    templateUrl: './city-dialog.component.html',
})

export class CityDialogComponent implements OnInit {
    @Input() cityDialogStatus: boolean
    cityDetail: FormGroup;
    @Output() cancelEvent = new EventEmitter();
    @Output() saveCityEvent = new EventEmitter();
    constructor(
        private _fb: FormBuilder,
    ) {
        this.cityDetail = this._fb.group({
            id: 0,
            title: ['', Validators.required],
            color: ['', Validators.required],
            text: ['', Validators.required]
        });
    }

    ngOnInit() {
    }

    cancel() {
        this.cancelEvent.emit(false)
    }

    saveCityDetail() {
        if (this.cityDetail.valid) {
            this.saveCityEvent.emit(this.cityDetail.value)
            this.cancel();
        }
    }

}

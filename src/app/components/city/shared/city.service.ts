import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map'

@Injectable({
    providedIn: 'root'
})
export class CityService {

    constructor(private http: Http) {
    }

    public getCityList(): Observable<any> {
        const url = ('../../../assets/json/city.json');
        return this.http.get(url).map(res => {
            return res.json()
        })
    }
}

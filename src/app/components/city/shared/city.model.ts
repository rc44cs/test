export class CityModel {
    id:number;
    color:string;
    text:string;
    title:string;
    active:boolean;
}
import { Routes, RouterModule } from '@angular/router';
import { CityComponent } from './city.component';



export const cityRoutes: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: CityComponent
      }
    ]
  }
];

export const routing = RouterModule.forChild(cityRoutes)



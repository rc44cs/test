
import { Router, NavigationEnd, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import { Component, OnInit, EventEmitter, Input, Output } from '@angular/core';
import { CityModel } from '../city/shared/city.model';

@Component({
    selector: 'app-sidebar',
    templateUrl: './sidebar.component.html',
})

export class SideBarComponent implements OnInit {
    @Input() city: Array<CityModel>;
    @Output() selectedCity = new EventEmitter();
    constructor() {
    }

    ngOnInit() {
        this.city.forEach(result => {
            result.active = false
        })
    }


    onSelectCity(row) {
        this.city.forEach(result => {
            result.active = false
        })
        row.active = true;
        this.selectedCity.emit(row)
    }

}
